# Flash the screen until the user presses a key
flasher () {
	while true; do
		printf "\\e[?5h"
		sleep 0.1
		printf "\\e[?5l"
		read -s -n1 -t1 && break
	done
}

# Update all git repositories in the current dir
update_all () {
	dir=${1:-${PWD}}
	for repo in $(find "$dir" -type d -depth 1); do
		if [ -e "$repo/.git" ]; then
			echo
			echo "=== Updating $repo"
			(cd $repo && git pull)
		fi
	done
}

# Clean all docker images matching given text (default "Exited")
docker_clean () {
	filter="${1:-Exited}"
	exited="$(docker ps -a | grep "${filter}" | awk '{ print $1 }')"
	if [[ "${exited}" ]]; then
		docker rm ${exited}
	fi

	dangling="$(docker images -q --filter "dangling=true")"
	if [[ "${dangling}" ]]; then
		docker rmi ${dangling}
	fi
}

docker_init () {
	machine="${1:-dev}"
	docker-machine start $machine
	eval "$(docker-machine env $machine)"
}

vilog () {
	file="~/Dropbox/Work/Daily/`date +'%Y.%m.%d.md'`"
	gvim --servername daily --remote-silent $file
}

greplog () {
	TAG="$1"
	LINES="${2:-2}"
	(cd ~/Dropbox/Work; ag -A${LINES} --no-numbers --color --group "#${TAG}")
}

lag () {
	ag --color --group $@ | less -R
}

tnw () {
	tmux new-window $@
}
# vim: ft=sh
